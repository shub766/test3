var crypto = require("crypto");
var path = require("path");
var fs = require("fs");
const key = crypto.randomBytes(32);
// console.log("Key",key)
const iv = crypto.randomBytes(16);
// var encryptStringWithRsaPublicKey = function(toEncrypt, relativeOrAbsolutePathToPublicKey) {
//     var absolutePath = path.resolve(relativeOrAbsolutePathToPublicKey);
//     var publicKey = fs.readFileSync(absolutePath, "utf8");
//     var buffer = Buffer.from(toEncrypt);
//     var encrypted = crypto.publicEncrypt(publicKey, buffer);
//     return encrypted.toString("base64");
// };

var decryptStringWithRsaPrivateKey = function(toDecrypt, relativeOrAbsolutePathtoPrivateKey) {
    var absolutePath = path.resolve(relativeOrAbsolutePathtoPrivateKey);
    var privateKey = fs.readFileSync(absolutePath, "utf8");
    var buffer = Buffer.from(toDecrypt, "base64");
    var decrypted = crypto.privateDecrypt(privateKey, buffer);
    return decrypted.toString("hex");
};

function firstshadecryption(encdata,rsadecryption){
    let hash = crypto
        .createHash('sha512')
        .update(encdata+rsadecryption)
        .digest('hex');
    return hash;
}

function decrypt(text) {
    let keybuffer = Buffer.from(text.aeskey, 'hex')
    console.log("in decrypt key",key)
    console.log("iv before buffer",text.iv)
    // let iv = crypto.randomBytes(32);
    let iv = Buffer.from(text.iv, 'hex');
    console.log("iv after buffer",iv)
    let encryptedText = Buffer.from(text.encryptedData, 'hex');
    console.log("key befor decipher",key)
    let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(keybuffer), iv);
    //  decipher.setAutoPadding(false);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}

function secondshadecryption(encdata,decryptedvalue){
    let hash = crypto
        .createHash('sha512')
        .update(encdata+decryptedvalue)
        .digest('hex');
    return hash;
}

// function decrypt(text) {
// //  let iv = Buffer.from(text.iv, 'hex').;
// // let iv = text.aeskey.substring(0,16);
// let iv = Buffer.from(text.iv, 'hex');
//  console.log("iv",iv);
//  console.log("Key",text.aeskey)
//  let encryptedText = Buffer.from(text.encryptedData, 'hex');
//  let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
//  decipher.setAutoPadding(false);
//  let decrypted = decipher.update(encryptedText);
//  decrypted = Buffer.concat([decrypted, decipher.final()]);
//  return decrypted.toString();
// }


// let filepath = path.join(__dirname,'.//certificates/credence_public.key')
// console.log(filepath)

// let rsaeskey = encryptStringWithRsaPublicKey("Hello world",filepath)

// console.log(rsaeskey);



let filepathone = path.join(__dirname,'.//certificates/credence.key')


const rsaeskey = 'fzVTL0ItZXtiYMMgy1fIT1wvTy+ypIpLspGxcPt6wedNRA3np6ITsn8lO0qoq6dI2NmES/yLPk5+nNKpyDSwfkO5SMb7FGnyZdAU6yBIFLE8GZqrEX2S5omE7Ko55EzrSGaU1kfZycP7HRjXSmB33WAJ3YXgmxXXtoA9o+lCSuu6bX1R3eU63ylNRI02OASPbKT42D+MZwvsmSyuORYdRRF/1zXRzOcWnvr+e8f5JPMmOkLLO4WSpKq3FVLFV/+inSszv7odwiI2CCXpSIa4Feyhnf+PKMqnbEwVc0DEUekUxgne1mtVnSly0d7FRR7CrRRUNYcQENn4ju7PcoDgAw=='

let rsadecrypt = decryptStringWithRsaPrivateKey(rsaeskey,filepathone)

console.log("step1",rsadecrypt);

let encdata = Buffer.from('1ee0679e964e3dab6ad2caffec979c5c', 'hex')
let keybuffer = Buffer.from(rsadecrypt, 'hex')
console.log("step 2",firstshadecryption(encdata,keybuffer))

obj = {
  iv: '4d0120244136c3a1fc0af36edafd33a9',
  encryptedData: '1ee0679e964e3dab6ad2caffec979c5c',
  aeskey : rsadecrypt
}

let decryptedvalue = decrypt(obj)
console.log("step3",decryptedvalue)

console.log("step 4",secondshadecryption(decryptedvalue,keybuffer))


// first iv <Buffer 4d 01 20 24 41 36 c3 a1 fc 0a f3 6e da fd 33 a9>
// encrypt key <Buffer 93 39 1e d4 4a 21 f6 3c c8 17 01 d8 74 84 52 89 37 49 40 15 6e 0a db d3 d9 d1 cc a3 96 d4 cb c8>       
// encrypt IV <Buffer 4d 01 20 24 41 36 c3 a1 fc 0a f3 6e da fd 33 a9>
// {
//   iv: '4d0120244136c3a1fc0af36edafd33a9',
//   encryptedData: '1ee0679e964e3dab6ad2caffec979c5c',
//   aeskey: <Buffer 93 39 1e d4 4a 21 f6 3c c8 17 01 d8 74 84 52 89 37 49 40 15 6e 0a db d3 d9 d1 cc a3 96 d4 cb c8>
// }
// in decrypt key <Buffer 93 39 1e d4 4a 21 f6 3c c8 17 01 d8 74 84 52 89 37 49 40 15 6e 0a db d3 d9 d1 cc a3 96 d4 cb c8>    
// iv before buffer 4d0120244136c3a1fc0af36edafd33a9
// iv after buffer <Buffer 4d 01 20 24 41 36 c3 a1 fc 0a f3 6e da fd 33 a9>
// key befor decipher <Buffer 93 39 1e d4 4a 21 f6 3c c8 17 01 d8 74 84 52 89 37 49 40 15 6e 0a db d3 d9 d1 cc a3 96 d4 cb c8>
// Aes decryption :)
// <Buffer 93 39 1e d4 4a 21 f6 3c c8 17 01 d8 74 84 52 89 37 49 40 15 6e 0a db d3 d9 d1 cc a3 96 d4 cb c8>
// RSAENCRYPTION  fzVTL0ItZXtiYMMgy1fIT1wvTy+ypIpLspGxcPt6wedNRA3np6ITsn8lO0qoq6dI2NmES/yLPk5+nNKpyDSwfkO5SMb7FGnyZdAU6yBIFLE8GZqrEX2S5omE7Ko55EzrSGaU1kfZycP7HRjXSmB33WAJ3YXgmxXXtoA9o+lCSuu6bX1R3eU63ylNRI02OASPbKT42D+MZwvsmSyuORYdRRF/1zXRzOcWnvr+e8f5JPMmOkLLO4WSpKq3FVLFV/+inSszv7odwiI2CCXpSIa4Feyhnf+PKMqnbEwVc0DEUekUxgne1mtVnSly0d7FRR7CrRRUNYcQENn4ju7PcoDgAw==
// HASH  20fc2dfdc29da050bbbaf0311d5d4e905c70f18f15916766066c12e14abad02f93a3bb5c0ebb9d71dd20dcca07f92b7b6a31d1df2a6399307e553abfbe04353b