const express = require('express')
const router = express.Router()
const crypto = require('crypto');
const NodeRSA = require('node-rsa');
const path = require("path");
const fs = require("fs");
const decryptionalgo = require('..//algorithms/decryptionalgo')

router.post('/aesrsa', async(req,res) => {
    try{

        const filepath = path.join(__dirname,'..//certificates/credence.key')

        const rsadecryption = decryptionalgo.decryptStringWithRsaPrivateKey(req.body.rsaAesKey,filepath)
        console.log("aeskey",rsadecryption);


        let hash = decryptionalgo.shadecryption(req.body.encData,rsadecryption)
        console.log("second hash",hash);


        // if(!(hash==req.body.signature)){
        //     res.json({'message':'Digital Signature mismatched.'})
        // }
        let aesdecryption = decryptionalgo.aesdecryption(req.body.encData);
        console.log("aesdecryption",aesdecryption);
        let shadecryption = decryptionalgo.shadecryption(aesdecryption,rsadecryption)

        obj = {
            "encData": hash, 
            "signature" : shadecryption,
            "aesKey" : rsadecryption,
            "rsaAesKey" : aesdecryption

        }
        res.json(obj)
    }catch(err){
        let er =  {
            "status":"success",
            "message":"Data decryption failed"
        }
        res.json(er)
    }
    

})

module.exports = router