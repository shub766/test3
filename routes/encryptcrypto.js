const express = require('express')
const router = express.Router()
const crypto = require('crypto');
const NodeRSA = require('node-rsa');
const path = require("path");
const fs = require("fs");
const encryptionalgo = require('..//algorithms/encryptionalgo')
    


router.post('/aesrsa', async(req,res) => {
    try{

        var gfg = encryptionalgo.aesencryption(req.body.plainText);
        console.log("gfg",gfg);

        let filepath = path.join(__dirname,'..//certificates/credence_public.key')

        let rsaeskey = encryptionalgo.encryptStringWithRsaPublicKey(gfg.Aeskey,filepath)
        console.log("rsaeskey",rsaeskey)

        const hash = encryptionalgo.shaencryption(gfg.encryptedData,gfg.Aeskey)

        obj = {
            "status":"success",
            "message":"Data encrypted",
            "encData": gfg, 
            "signature" : hash,
            "aesKey" : gfg.Aeskey.toString('hex'),
            "rsaAesKey" : rsaeskey
        }
        res.json(obj)

    }catch(err){
        let er =  {
            "status":"success",
            "message":"Data encryption failed"
        }
        res.json(er)
    }
})

module.exports = router 