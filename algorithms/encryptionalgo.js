const express = require('express')
const router = express.Router()
const crypto = require('crypto');
const NodeRSA = require('node-rsa');
const path = require("path");
const fs = require("fs");

function aesencryption(text){
    console.log("IN aesencryption");
    const algorithm = 'aes-256-cbc';
    const key = crypto.randomBytes(32);
    console.log("Key",key)
    const iv = crypto.randomBytes(16);
    // function encrypt(text) {
    let cipher = crypto.createCipheriv('aes-256-cbc',Buffer.from(key), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    console.log("encrypted",encrypted)
    return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex'),Aeskey:key.toString('hex')};
    
}

function getpublickey(relativeOrAbsolutePathToPublicKey){
    var absolutePath = path.resolve(relativeOrAbsolutePathToPublicKey);
    var publicKey = fs.readFileSync(absolutePath, "utf8");
    return publicKey;

}

const encryptStringWithRsaPublicKey = function(toEncrypt, relativeOrAbsolutePathToPublicKey) {
    var publicKey = getpublickey(relativeOrAbsolutePathToPublicKey)
    var buffer = Buffer.from(toEncrypt);
    var encrypted = crypto.publicEncrypt(publicKey, buffer);
    return encrypted.toString("base64");
}

function shaencryption(encdata,aeskey){
    let hash = crypto
        .createHash('sha512')
        .update(encdata+aeskey)
        .digest('hex');
    return hash;
}

module.exports = {
    aesencryption,getpublickey,encryptStringWithRsaPublicKey,shaencryption

}



