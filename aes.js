const crypto = require('crypto');

console.log(crypto.getCurves());

const alice = crypto.createECDH('secp256k1');
alice.generateKeys();

const bob = crypto.createECDH('secp256k1');
bob.generateKeys();

const alicepublickeybase64 = alice.getPublicKey().toString('base64')
const bobpublickeybase64 = bob.getPublicKey().toString('base64')

const alicesharedkey = alice.computeSecret(bobpublickeybase64,'base64','hex')
const bobsharedkey = bob.computeSecret(alicepublickeybase64,'base64','hex')

console.log(alicesharedkey==bobsharedkey)
const aes256 = require('aes256')

const message = 'This is secret key';

const encryptedvalue =  aes256.encrypt(alicesharedkey,message);
console.log(encryptedvalue)


const decryptedvalue =  aes256.decrypt(bobsharedkey,encryptedvalue);
console.log(decryptedvalue)



// const crypto = require('crypto');
// const algorithm = 'aes-256-cbc';
// const key = crypto.randomBytes(32);
// const iv = crypto.randomBytes(16);
  
// function encrypt(text) {
// let cipher = crypto.createCipheriv('aes-256-cbc',Buffer.from(key), iv);
// let encrypted = cipher.update(text);
// encrypted = Buffer.concat([encrypted, cipher.final()]);
// return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
// }
  
// function decrypt(text) {
// let iv = Buffer.from(text.iv, 'hex');
// let encryptedText = Buffer.from(text.encryptedData, 'hex');
// let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
// let decrypted = decipher.update(encryptedText);
// decrypted = Buffer.concat([decrypted, decipher.final()]);
// return decrypted.toString();
// }
  
// var gfg = encrypt(":)");
// console.log(gfg);

// var decrptedvalue = decrypt(gfg);
// console.log(decrptedvalue)